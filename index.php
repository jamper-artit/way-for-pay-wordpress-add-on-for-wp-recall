<?php

if (class_exists('Rcl_Payment')) {

add_action('init','rcl_add_wayforpay_payment');
function rcl_add_wayforpay_payment(){
    $wfp = new Rcl_WFP_Payment();
    $wfp->register_payment('way-for-pay');
}

class Rcl_WFP_Payment extends Rcl_Payment{

    public $form_pay_id;

    function register_payment($form_pay_id){
        $this->form_pay_id = $form_pay_id;
        parent::add_payment($this->form_pay_id, array(
            'class'=>get_class($this),
            'request'=>'WFP_TYPE_PAY',
            'name'=>'Way For Pay',
            'image'=>rcl_addon_url('assets/way-for-pay.jpg',__FILE__)
            ));
        if(is_admin()) $this->add_options();
    }

    function add_options(){
        add_filter('rcl_pay_option',(array($this,'options')));
        add_filter('rcl_pay_child_option',(array($this,'child_options')));
    }

    function options($options){
        $options[$this->form_pay_id] = 'Way For Pay';
        return $options;
    }

    function child_options($child){
        global $rmag_options;

        $opt = new Rcl_Options();

        $curs = array( 'USD', 'EUR', 'UAH' );

        if(false !== array_search($rmag_options['primary_cur'], $curs)) {
            $options = array(
                array(
                    'type' => 'text',
                    'slug' => 'wfp_account',
                    'title' => __('Merchant login (Например U123456)')
                ),
                array(
                    'type' => 'text',
                    'slug' => 'wfp_site_name',
                    'title' => __('Адрес сайта')
                ),
                array(
                    'type' => 'text',
                    'slug' => 'wfp_phrase',
                    'title' => __('Merchant secret key')
                )
            );
        }else{
            $options = array(
                array(
                    'type' => 'custom',
                    'slug' => 'notice',
                    'notice' => __('<span style="color:red">Данное подключение не поддерживает действующую валюту сайта.<br>'
                        . 'Поддерживается работа с USD, EUR, UAH</span>')
                )
            );
        }

        $child .= $opt->child(
            array(
                'name'=>'connect_sale',
                'value'=>$this->form_pay_id
            ),
            array(
                $opt->options_box( __('Настройки подключения Way For Pay'), $options)
            )
        );

        return $child;
    }

    function pay_form($data){
        global $rmag_options;

        $wpf_account = $rmag_options['wfp_account'];
        $wfp_site_name = $rmag_options['wfp_site_name'];
        $wfp_phrase = $rmag_options['wfp_phrase'];

        $currency = $rmag_options['primary_cur'];
        $timestamp = current_time('timestamp');
        
        $user = wp_get_current_user();
        if(empty($data->currency))
        {
	        $data->currency = 'USD';
        }

        $string = $wpf_account.';'.$wfp_site_name.';'.$data->pay_id.';'.$timestamp.';'.$data->pay_summ.';'.$data->currency.';'.$data->description.';'.'1'.';'.$data->pay_summ;
        $key = $wfp_phrase;
        $hash = hash_hmac("md5",$string,$key);

        $fields = array(
            'merchantAccount' => $wpf_account,
            'merchantAuthType' => 'SimpleSignature',
            'merchantDomainName' => $wfp_site_name,
            'orderReference' => $data->pay_id,
            'orderDate' => $timestamp,
            'amount' => $data->pay_summ,
            'currency' => $data->currency,
            'alternativeCurrency' => 'USD',
            'productName' => $data->description,
            'productPrice' => $data->pay_summ,
            'productCount' => 1,
            'clientFirstName' => $user->data->user_nicename,
            'clientLastName' => '',
            'clientAddress' => '',
            'clientCity' => '',
            'clientEmail' => $user->data->user_email,
            'defaultPaymentSystem' => 'card',
            'merchantSignature' => $hash,
            'returnUrl' => get_permalink($rmag_options['page_result_pay']),
            'serviceUrl' => get_permalink($rmag_options['page_result_pay'])
        );

        $form = parent::form($fields,$data,"https://secure.wayforpay.com/pay");

        return $form;
    }

    function result($data){
        global $rmag_options;

        $wfp_phrase = $rmag_options['wfp_phrase'];

        define('ALTERNATE_PHRASE_HASH', strtoupper(md5($wfp_phrase)));

        $array = array(
            $_REQUEST['PAYMENT_ID'],
            $_REQUEST['PAYEE_ACCOUNT'],
            $_REQUEST['PAYMENT_AMOUNT'],
            $_REQUEST['PAYMENT_UNITS'],
            $_REQUEST['PAYMENT_BATCH_NUM'],
            $_REQUEST['PAYER_ACCOUNT'],
            ALTERNATE_PHRASE_HASH,
            $_REQUEST['TIMESTAMPGMT']
        );

        $hash = strtoupper(md5(implode(':',$array)));

        if($hash!=$_REQUEST['V2_HASH']){
            rcl_mail_payment_error($hash);
            die('HASH failed.');
        }

        $data->pay_summ = $_REQUEST['PAYMENT_AMOUNT'];
        $data->pay_id = $_REQUEST['PAYMENT_ID'];
        $data->user_id = $_REQUEST['PM_USER_ID'];
        $data->pay_type = $_REQUEST['PM_TYPE_PAY'];
        $data->baggage_data = $_REQUEST['PM_BAGGAGE_DATA'];

        if(!parent::get_pay($data)){
            parent::insert_pay($data);
            die('PAYMENT OK.');
        }
    }

    function success(){
        global $rmag_options;

        $data['pay_id'] = $_REQUEST['PAYMENT_ID'];
        $data['user_id'] = $_REQUEST['PM_USER_ID'];

        if(parent::get_pay((object)$data)){
            wp_redirect(get_permalink($rmag_options['page_successfully_pay'])); exit;
        } else {
            wp_die('Платеж не найден в базе данных');
        }

    }

}

}
